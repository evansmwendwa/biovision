<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;
use AppBundle\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use PHPHtmlParser\Dom;

class FinderCommand extends ContainerAwareCommand
{
    protected $root;
    protected $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        $this->root = realpath(__DIR__.'/../../../bv');
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('biovision:finder')
            ->setDescription('Index Biovision Files')
            ->setHelp('This command creates an index of the biovision files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if($this->root == false) {
            $output->writeln([
                '',
                '',
                '=========================',
                '       Path Error!       ',
                '=========================',
                '',
            ]);
            return;
        }

        $output->writeln([
            '',
            '',
            '=========================',
            '       Beginx Index       ',
            '=========================',
            '',
        ]);

        $finder = new Finder();
        $finder->files()->name('*.html')->in($this->root);

        foreach ($finder as $file) {
            try {

                if (!$this->em->isOpen()) {
                    $this->em = $this->em->create(
                        $this->em->getConnection(),
                        $this->em->getConfiguration()
                    );
                }

                $output->writeln([
                    '',
                    '+ '.$file->getRelativePathname(),
                    ''
                ]);

                $contents = $file->getContents();

                $dom = new Dom;

                $dom->load($contents);

                $title = strip_tags($dom->find('title')[0]);
                $body = $dom->find('body')[0];

                $f = new File();
                $f->setTitle($title);
                $f->setRealPath($file->getRealPath());
                $f->setRelativePath($file->getRelativePath());
                $f->setRelativePathname($file->getRelativePathname());
                $f->setHtmlContent('-');
                $f->setPlainText(strip_tags($body));

                $this->em->persist($f);
                $this->em->flush();

                $output->writeln([
                    '',
                    '> Done! ',
                ]);

            } catch(\Exception $e) {
                $output->writeln('ERROR!: '.  $e->getMessage(). "\n");
                $output->writeln($file->getRealPath());
                file_put_contents(__DIR__.'/../../../failedfiles.log', $file->getRealPath().PHP_EOL , FILE_APPEND );
            }

        }

        $output->writeln([
            '',
            '=========================',
            '   Index Job Completed   ',
            '=========================',
            '',
        ]);
    }
}
