<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* AppBundle\Entity\file
* @ORM\Entity
* @ORM\Table(name="indexed_files")
* @ORM\HasLifecycleCallbacks()
*/
class File
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     */
    protected $realPath;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     */
    private $relativePath;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     */
    private $relativePathname;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $htmlContent;

    /**
     * @ORM\Column(type="text")
     */
    private $plainText;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;





    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Real Path
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Real Path
     *
     * @param mixed realPath
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Real Path
     *
     * @return mixed
     */
    public function getRealPath()
    {
        return $this->realPath;
    }

    /**
     * Set the value of Real Path
     *
     * @param mixed realPath
     *
     * @return self
     */
    public function setRealPath($realPath)
    {
        $this->realPath = $realPath;

        return $this;
    }

    /**
     * Get the value of Relative Path
     *
     * @return mixed
     */
    public function getRelativePath()
    {
        return $this->relativePath;
    }

    /**
     * Set the value of Relative Path
     *
     * @param mixed relativePath
     *
     * @return self
     */
    public function setRelativePath($relativePath)
    {
        $this->relativePath = $relativePath;

        return $this;
    }

    /**
     * Get the value of Relative Pathname
     *
     * @return mixed
     */
    public function getRelativePathname()
    {
        return $this->relativePathname;
    }

    /**
     * Set the value of Relative Pathname
     *
     * @param mixed relativePathname
     *
     * @return self
     */
    public function setRelativePathname($relativePathname)
    {
        $this->relativePathname = $relativePathname;

        return $this;
    }

    /**
     * Get the value of Html Content
     *
     * @return mixed
     */
    public function getHtmlContent()
    {
        return $this->htmlContent;
    }

    /**
     * Set the value of Html Content
     *
     * @param mixed htmlContent
     *
     * @return self
     */
    public function setHtmlContent($htmlContent)
    {
        $this->htmlContent = $htmlContent;

        return $this;
    }

    /**
     * Get the value of Plain Text
     *
     * @return mixed
     */
    public function getPlainText()
    {
        return $this->plainText;
    }

    /**
     * Set the value of Plain Text
     *
     * @param mixed plainText
     *
     * @return self
     */
    public function setPlainText($plainText)
    {
        $this->plainText = $plainText;

        return $this;
    }

    /**
     * Get the value of Created At
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     *
     * @return this
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTimeImmutable();
        return $this;
    }

    /**
     * Get the value of Updated At
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PreUpdate
     * @ORM\PrePersist
     *
     * @return HtmlModule
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

}
